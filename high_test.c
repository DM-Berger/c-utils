#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>

#include "dmb_utils.h"

#include "high_test.h"
// #include "ht_channel.h"

const static size_t MAX_CHILD_PROCESSES = 8;
const static size_t SUMMARY_LENGTH = 512;
const static size_t STDERR_CAPTURE_LENGTH = 8192; // about 100 lines I guess...
const static size_t STDERR_LINES_MAX = 10; 

typedef struct _t_type {
    bool declared;
    bool should_exit;
    bool should_sigexit;
    int code;
} t_type;

typedef struct _sfork_results_t {
    bool done;
    pid_t pid;
    int status; 
} sfork_res_t;

typedef struct _t_info {
    t_fn function;
    char* label;
    t_type type;
    sfork_res_t results;
    char* summary;
    bool printed;
} t_info;

typedef struct _Channel {
    pid_t pid;
    bool available;
    int recv;
    int send;
    char buf[STDERR_CAPTURE_LENGTH];
} Channel;

// types related to safer forking functions 
typedef void(*void_f)(void);


// static globals
static size_t TOTAL_TESTS = 0;
static size_t MAX_TESTS = 64;
static size_t TEST_REPEATS = 1;
static t_info* TESTS = NULL;

static pid_t ROOT_PID;
static Channel CHANNELS[MAX_CHILD_PROCESSES];
static int DRAIN;


/******************************************************************************
***                                Interface                                 ***
******************************************************************************/
void should_exit_with_code(char* label, t_fn f, int code);
void should_exit_with_signal(char* label, t_fn f, int signal);
void should_just_pass(char* label, t_fn f);
void should_segfault(char* label, t_fn f);
void run_tests();
void set_test_repeats(unsigned count);

/******************************************************************************
 ***                                 Setup                                  ***
 ******************************************************************************/
static void initialize_testing_environment(void);
static void push_test_info(t_info info);

/******************************************************************************
***                                Execution                                 ***
******************************************************************************/
static void execute_in_new_process(t_info* test);
// static void execute_in_new_process(t_fn test);

/******************************************************************************
***                             Post-Processing                              ***
******************************************************************************/
static t_results test_function(t_fn f, size_t times);
static int test_and_get_exit_status(t_fn f, char* label, unsigned times);
static void summarize_test_status(t_info* test);

/******************************************************************************
 ***                          Printing/Formatting                           ***
 ******************************************************************************/
static char* format_test_start_info(char* label);
static void print_test_completion_info(t_results);

/******************************************************************************
 ***                      Helpers and fork() Wrappers                       ***
 ******************************************************************************/

// Helpers
static void push_pid(pid_t pid);
static sfork_res_t init_sfork_result(void);
static t_info init_t_info(void);


// safe fork wrappers
static pid_t sfork_run(void f(void));
static void wait_and_print_a_result(t_info* tests);
static void sfork_reap_remaining(t_info* tests);
static void sfork_run_all_limited(t_info* tests);
static size_t count_done(t_info* tests, size_t);

/******************************************************************************
 ***                           Channels and Pipes                            ***
 ******************************************************************************/
static Channel init_channel(void);
static Channel* open_a_channel(void);
static void register_pid(Channel* channel, pid_t pid);
static void set_channel_to_recv(pid_t child);
static void set_channel_to_send(pid_t child);
static void close_channel(pid_t pid);
static void reset_channel(pid_t pid);

// Channel helpers
static Channel* get_channel(pid_t pid);
static void err_close(Channel* channel, int end);
static void LOGchannelinfo(void);

// Output redirection
static void swallow_child_stdout(void);
static void capture_stderr(pid_t pid);
static void get_stderr_output(char* buf, size_t bufsize, pid_t finished);
static void print_stderr_output(char* output, size_t length, size_t linemax);


/*****************************************************************************
 *****************************************************************************
 *****************************************************************************/
void should_exit_with_code(char* label, t_fn f, int code) {
    t_type kind = {
        .declared = true,
        .should_exit = true,
        .should_sigexit = false,
        .code = code
    };
    char* summary = format_test_start_info(label);
    t_info f_info = {f, label, kind, init_sfork_result(), summary, false };
    push_test_info(f_info);
}
void should_exit_with_signal(char* label, t_fn f, int signalcode) {
    t_type kind = {
        .declared = true,
        .should_exit = false,
        .should_sigexit = true,
        .code = signalcode
    };
    char* summary = format_test_start_info(label);
    t_info f_info = {f, label, kind, init_sfork_result(), summary, false };
    push_test_info(f_info);
}
void should_just_pass(char* label, t_fn f) {
    should_exit_with_code(label, f, 0);
}
void should_segfault(char* label, t_fn f) {
    should_exit_with_signal(label, f, SIGSEGV);
}
void should_abort(char* label, t_fn f) {
    should_exit_with_signal(label, f, SIGABRT);
}
void set_test_repeats(unsigned count) {
    TEST_REPEATS = count;
}

/*****************************************************************************
 ***                               Testing Setup                           ***
 *****************************************************************************/

// make room for a reasonable amount of tests
static void initialize_testing_environment(void) {
    static bool initialized = false;
    if (!initialized) {
        initialized = true;
    } else {
        LOGERROR("Error: run_tests() may only be called once.");
        exit(EXIT_FAILURE);
    }
    
    if (NULL == (TESTS = (t_info*) malloc( MAX_TESTS*sizeof(t_info) ))) {
        LOGERROR("Failed to allocate space for tests.");
        exit(1);
    }
    ROOT_PID = getpid();
    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
        CHANNELS[i] = init_channel();
    }
    if ((DRAIN = open("/dev/null", O_WRONLY)) < 0) {
        LOGEXIT("Failed to open /dev/null");
    }
    // for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
    //     fprintf(stderr, "Channel buffer has in it: %s\n", CHANNELS[i].buf);
    // }
    
}

static void push_test_info(t_info info) {
    if (TOTAL_TESTS == 0) {
        initialize_testing_environment();
    }
    
    if (TOTAL_TESTS < MAX_TESTS) {
        TESTS[TOTAL_TESTS] = info;
        TOTAL_TESTS++;
    } else if (TOTAL_TESTS == MAX_TESTS) {
        fprintf(stderr, "Reallocating space for more tests.\n");
        MAX_TESTS *= 2;
        if (NULL == (TESTS = (t_info*) realloc(TESTS, MAX_TESTS*sizeof(t_info)))) {
            LOGERROR("Error reallocating tests array");
            exit(EXIT_FAILURE);
        }
        TESTS[TOTAL_TESTS] = info;
        TOTAL_TESTS++;
    } else {
        LOGERROR("Error pushing to TESTS array.");
        exit(1);
    }
}

/*****************************************************************************
 ***                               Test Execution                           ***
 *****************************************************************************/

void run_tests(void) {
    // would make a lot of sense to have this in it's own thread!
    // the t_info* TESTS object would have to be behind a mutex
    sfork_run_all_limited(TESTS);
    free(TESTS);
    

    // likewise the summarization and printing could be a separate thread

    // Alternately, wrap summarization and printing into sfork_run_all_limited
}


// note that init_t_info assumes that the test result strings will be less than
// 256 characters - if they are particularly long, the sprintf will write to un-
// allocated memory, and thus have undefined behaviour
static void summarize_test_status(t_info* test) {
     extern const char * const sys_siglist[];
    if (!(test->type.declared)) {
        LOGEXIT("high_test failed to initialize test correctly");
    }

    int status = test->results.status;
    char summary[SUMMARY_LENGTH];
    if (test->type.should_exit) {
        if (!WIFEXITED(status)) {
            snprintf(summary, SUMMARY_LENGTH, "..." RED "FAIL" RESET ".\n\tReason: terminated with signal " YEL "%d" RESET " (%s), expected exit code: " YEL "%d" RESET "\n", WTERMSIG(status), sys_siglist[WTERMSIG(status)], test->type.code);
        } else {
            if (WEXITSTATUS(status) != test->type.code) {
                snprintf(summary, SUMMARY_LENGTH, "..." RED "FAIL" RESET ".\n\tReason: exited with code " YEL "%d" RESET ", expected exit code: " YEL "%d" RESET "\n", WEXITSTATUS(status), test->type.code);
                } else {
                snprintf(summary, SUMMARY_LENGTH, "..." GRN "PASS" RESET "!");
            }
        }
    } else if (test->type.should_sigexit) {
        if (!WIFSIGNALED(status)) {
            snprintf(summary, SUMMARY_LENGTH, "..." RED "FAIL" RESET ".\n\tReason: exited with code " YEL "%d" RESET ", expected signal: " YEL "%d" RESET "\n", WEXITSTATUS(status), test->type.code);
            return;
        } else {
            int sig = WTERMSIG(status);
            if (sig != test->type.code) {
                snprintf(summary, SUMMARY_LENGTH, "..." RED "FAIL" RESET ".\n\tReason: terminated with signal " YEL "%d" RESET " (%s), expected signal: " YEL "%d" RESET "\n", sig, sys_siglist[sig], test->type.code);
            } else {
                snprintf(summary, SUMMARY_LENGTH, "..." GRN "PASS" RESET "!");
            }
        }
    } else {
        // because of initialization of t_info types to all false, only way we
        // can reach here is if both test.type.should_exit and test.type.
        // should_sigexit are false. 
        LOGEXIT("Test has conflicting exit conditions.");
    }
    strcat(test->summary, summary);
}

/*****************************************************************************
 ***                                 Formatting                            ***
 *****************************************************************************/

// caller is responsible for freeing returned string
static char* format_test_start_info(char* label) {
    char* out = (char*) malloc( SUMMARY_LENGTH*sizeof(char) );
    
    snprintf(out, SUMMARY_LENGTH, "Testing " BLU "%s" RESET "...", label);
    // measure actual output length after labels and count info
    size_t out_len = strlen(out);
    size_t color_len = strlen(BLU);
    size_t reset_len = strlen(RESET);
    out_len -= color_len;
    out_len -= reset_len;

    // pad output with .... chars to termwidth
    unsigned termwidth = get_term_width();
    size_t pad_width = 0;
    if (termwidth >= out_len + 10) {
        pad_width = (size_t) termwidth - out_len - 10;
    }
    if (pad_width != 0) {
        char *padding = malloc(pad_width + 1);
        memset(padding, '.', pad_width);
        padding[pad_width] = '\0';
        strcat(out, padding);
        free(padding);
    }
    return out;
}

static void print_completed_summary(t_info* test) {
    if (test->results.done && !(test->printed)) {
        printf("%s\n", test->summary);
        test->printed = true;
    } else {
        LOGEXIT("Test has already been printed or is not done.");
    }
    
}


/******************************************************************************
 ***                      Helpers and fork() Wrappers                       ***
 ******************************************************************************/


t_results new_results(void) {
    t_results results;
    results.pass = FAIL;
    results.succ = 0;
    results.fail = 0;
    results.status = 0;
    return results;
}

static sfork_res_t init_sfork_result(void) {
    sfork_res_t result = { false, -1, -666 };
    return result;
}

static t_info init_t_info(void) {
    t_type kind = {
        .declared = false,
        .should_exit = true,
        .should_sigexit = false,
        .code = -666
    };
    sfork_res_t results = { false, -1, -666 };
    t_info info;
    info.function = NULL;
    info.label = NULL;
    info.results = results;
    info.type = kind;
    info.summary = (char*) malloc(SUMMARY_LENGTH * sizeof(char));
    info.printed = false;
    return info;
}

// ensures that only the main root parent process will fork a new process
// doesn't completely prevent a fork bomb (in theory, if the function passed in
// does some evil longjmp shenanigans, or if passed-in-function itself calls the
// actual unlimited fork() function) but certainly makes it a lot harder to do
// so accidentally
static pid_t sfork_run(void f(void)) {


    // sleep(1);

    const static u_int8_t SFORK_LIMIT = 64; // doesn't prevent fork bomb
    static size_t TOTAL_PROCESSES_FORKED = 0;

    if (TOTAL_PROCESSES_FORKED >= SFORK_LIMIT) {
        if (getpid() == ROOT_PID)
            fprintf(stderr, "Reached sfork_run limit of %hhu total forked processes, terminating all processes.\n", SFORK_LIMIT);
        exit(EXIT_FAILURE);
    }

    // flush buffers in case a printf or w/e hasn't been flushed
    fflush(stderr);
    fflush(stdout);
    Channel* opened = open_a_channel();
    if (getpid() == ROOT_PID) { // ensure only the root process actually forks
        pid_t pid = fork();
        if (pid == (pid_t)-1) {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        if (pid == (pid_t)0) { // root's child process
            pid_t child = getpid();
            register_pid(opened, child);
            // LOGchannelinfo();
            set_channel_to_send(child);
            swallow_child_stdout();
            capture_stderr(child);
            f();
            exit(0); // exit closes last open send pipe/channel
        } else { // root process
            register_pid(opened, pid);
            // LOGchannelinfo();
            TOTAL_PROCESSES_FORKED++;
            return pid; // return pid of child process to main parent only
        }
    } else {
        exit(0); // make sure all other child processes immediately exit
    }
}

static void sfork_run_all_limited(t_info* tests) {
    size_t running = 0;
    for (size_t i = 0; i < TOTAL_TESTS; i++) {
        tests[i].results.pid = sfork_run(tests[i].function);
        running++;
        if (running >= MAX_CHILD_PROCESSES) {
            wait_and_print_a_result(tests); // won't run if tests small!
            running = TOTAL_TESTS - count_done(tests, i + 1);
        }
    }
    sfork_reap_remaining(tests);
}

static void sfork_reap_remaining(t_info* tests) {
    size_t done = count_done(tests, TOTAL_TESTS);
    while (done++ < TOTAL_TESTS)
        wait_and_print_a_result(tests);
}

// this fellow is also going to have to grab anything in the channel buffer,
// print it, and then free up the channel for the next process
static void wait_and_print_a_result(t_info* tests) {
    // wait for whatever test is first to finish and save the status
    int status_temp;
    pid_t finished = waitpid(-1, &status_temp, 0);
    if (finished == -1) {
        LOGERROR("waitpid error");
        exit(1);
    }
    char output[STDERR_CAPTURE_LENGTH] = "\0";
    err_close(get_channel(finished), 1);         // close root channel send end
    get_stderr_output(output, STDERR_CAPTURE_LENGTH, finished); // read from channel
    err_close(get_channel(finished), 0);         // close root channel recv end
    reset_channel(finished);
    

    // update results data for the finished test
    for (size_t i = 0; i < TOTAL_TESTS; i++) {
        if (tests[i].results.pid == finished) {
            tests[i].results.status = status_temp; 
            tests[i].results.done = true;
            summarize_test_status(&tests[i]);
            print_completed_summary(&tests[i]);
            print_stderr_output(output, STDERR_CAPTURE_LENGTH, STDERR_LINES_MAX);
        }
    }
}

static size_t count_done(t_info* tests, size_t last_executed) {
    size_t done = 0; 
    for (size_t i = 0; i < last_executed; i++) {
        if (tests[i].results.done) { done++; }
    }
    return done;
}

/******************************************************************************
 ***                          Channels and Pipes                            ***
 ******************************************************************************/

static Channel init_channel(void) {
    Channel c = {
        .pid = (pid_t)-1,
        .available = true,
        .recv = -1,
        .send = -1,
    };
    return c;
}

// associate a new pipe with a channel
static Channel* open_a_channel(void) {
    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
        if (CHANNELS[i].available) {
            int inner_pipe[2];
            if (pipe(inner_pipe) == -1)
                LOGEXIT("pipe failure");

            // ensure excess writing to pipe will not hang everything
            int flags;
            if ((flags = fcntl(inner_pipe[1], F_GETFL, 0)) < 0)
                LOGEXIT("Error getting pipe status flags");
            flags |= O_NONBLOCK;
            if (fcntl(inner_pipe[1], F_SETFL, flags) == -1)
                LOGEXIT("Error setting pipe status flags");

            CHANNELS[i].available = false;
            CHANNELS[i].recv = inner_pipe[0];
            CHANNELS[i].send = inner_pipe[1];
            return &CHANNELS[i];
        }
        // throw if don't find an available channel
        if ((i == MAX_CHILD_PROCESSES - 1) && (!CHANNELS[i].available))
            LOGEXIT("Too many channels in use.");
    }
    LOGEXIT("UNREACHABLE");
}

// this function must be called in both the root and child to ensure that
// the CHANNELS array is synchronized across processes
static void register_pid(Channel* channel, pid_t pid) {
    if (channel->pid != -1)
        LOGEXIT("Channel already has pid regestered");
    channel->pid = pid;
}

// root should only ever be closing one pipe send end at a time
// THIS IS THE PROBLEM, basically it closes the send ends of the pipes, and this
// frees the fd associated with that end somehow, so that a new pipe can use it
// but we do NOT want that
static void set_channel_to_recv(pid_t child) {
    if (getpid() != ROOT_PID)
        LOGEXIT("Child should not be calling this function");
    
    err_close(get_channel(child), 1);
}

// child must close all other pipes it isn't using (they get duped with fork)
// if not, read() calls in the main process will block
static void set_channel_to_send(pid_t child) {
    if (getpid() == ROOT_PID) // only child should ever be calling this function
        LOGEXIT("Root process should not be calling this function");

    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
        if (CHANNELS[i].pid != -1) {
            err_close(&CHANNELS[i], 0);   // close all recv. ends
            if (CHANNELS[i].pid != child)
                err_close(&CHANNELS[i], 1); // close other send ends
        }
    }
}

// must be called after waiting on process pid, but before reading from the pipe
static void close_channel(pid_t pid) {
    if (getpid() != ROOT_PID)
        LOGEXIT("Child should not be calling this function");
    err_close(get_channel(pid), 0);
    err_close(get_channel(pid), 1);
}

// must be called in 
static void reset_channel(pid_t pid) {
    Channel* channel = get_channel(pid);
    channel->available = true;
    channel->pid = -1;
}



static Channel* get_channel(pid_t pid) {
    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++)
        if (CHANNELS[i].pid == pid)
            return &CHANNELS[i];
    
    char message[128];
    sprintf(message, "No associated channel for pid %d", pid);
    LOGEXIT(message);
}

static void err_close(Channel* channel, int end) {
    pid_t pid = getpid();
    char pid_string[128];
    if (pid == ROOT_PID) {
        sprintf(pid_string, "Root process");
    } else {
        sprintf(pid_string, "Child process %d", pid);
    }
    if (end == 1) { // send end of pipe
        if (close(channel->send) == -1) {
            char message[256];
            sprintf(message, "%s failed to close channel send end (fd: %d, pid: %d)", pid_string, channel->send, channel->pid);
            LOGEXIT(message);
        }
    } else if (end == 0) { // recv end of pipe
        if (close(channel->recv) == -1) {
            char message[256];
            sprintf(message, "%s failed to close channel recv end (fd: %d, pid: %d)", pid_string, channel->recv, channel->pid);
            LOGEXIT(message);
        }
    } else {
        LOGEXIT("Can only close either 'send' (1) or 'recv' (0) end.");
    }
    fflush(stderr);
}

static void LOGchannelinfo(void) {
    pid_t pid = getpid();
    char process[128];
    if (pid == ROOT_PID) {
       sprintf(process, "ROOT");
    } else {
        sprintf(process, "CHILD %d", pid);
    }
    fflush(stderr); 
    fprintf(stderr, "%s CHANNEL pids: {", process);
    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
        if (i == MAX_CHILD_PROCESSES - 1)
            fprintf(stderr, "%d}\n", CHANNELS[i].pid);
        else 
            fprintf(stderr, "%d,", CHANNELS[i].pid);
    }
    fprintf(stderr, "%s CHANNELS.send: {", process);
    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
        if (i == MAX_CHILD_PROCESSES - 1)
            fprintf(stderr, "%d}\n", CHANNELS[i].send);
        else 
            fprintf(stderr, "%d,", CHANNELS[i].send);
    }
    fprintf(stderr, "%s CHANNELS.recv: {", process);
    for (size_t i = 0; i < MAX_CHILD_PROCESSES; i++) {
        if (i == MAX_CHILD_PROCESSES - 1)
            fprintf(stderr, "%d}\n", CHANNELS[i].recv);
        else 
            fprintf(stderr, "%d,", CHANNELS[i].recv);
    }
    fflush(stderr); 
}

/******************************************************************************
 ***                           Output Redirection                           ***
 ******************************************************************************/

static void swallow_child_stdout(void) {
    if (dup2(DRAIN, STDOUT_FILENO) == -1)
        LOGEXIT("Child failed to swallow stdout for test");
}

static void capture_stderr(pid_t pid) {
    Channel* channel = get_channel(pid);
    if (dup2(channel->send, STDERR_FILENO) == -1) {
        char message[256];
        sprintf(message, "Child %d failed to dup STDERR to channel send (bad fd: %d)", pid, channel->send);
        LOGEXIT(message);
    }
}

static void get_stderr_output(char* buf, size_t bufsize, pid_t finished) {
    Channel* channel = get_channel(finished);
    // fprintf(stderr, "Getting to read()\n");
    ssize_t res = read(channel->recv, buf, bufsize);
    if (strnlen(buf, bufsize) == bufsize)
        buf[bufsize - 1] = '\0';  // play it safe just in case
    // fprintf(stderr, "Getting past read()\n");
    if (res == -1) {
        if (errno == EINTR) LOGERROR("Signal interrupted reading from channel");
        else LOGEXIT("Error reading stderr output from pipe");
    }
}

static void print_stderr_output(char* output, size_t length, size_t linemax) {
    if (output[0] == '\0')
        return;
    if (strnlen(output, length) == length - 1) {
        fprintf(stderr, YEL "Warning" RESET ": stderr capture limit (%zu char) exceeded. Truncating to first %zu lines:\n", STDERR_CAPTURE_LENGTH, linemax);
        char* line;
        size_t count = 0;
        while (((line = strsep(&output, "\n")) != NULL) && (++count <= linemax))
        {
            printf("\t%s\n", line);
        }
        printf("\n");
    } else {
        char* line;
        while (((line = strsep(&output, "\n")) != NULL)) {
            printf("\t%s\n", line);
        }
    }
    //     return;
}