#ifndef SFORK_H
#define SFORK_H

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "high_test.h"
#include "dmb_utils.h"

typedef void(*null_f_ptr_t)(void);

typedef struct _sfork_results_t {
    bool done;
    pid_t pid;
    int status; 
} sfork_res_t;

static pid_t sfork( void );
static pid_t sfork_run(void f(void));
static sfork_res_t* sfork_init_results(size_t test_count);
static void sfork_run_all(null_f_ptr_t* functions, sfork_res_t* results, size_t count);
static void wait_for_one_to_finish(sfork_res_t* results, size_t count);
static void sfork_reap_remaining(sfork_res_t* results, size_t count);
static size_t sfork_reap_n(sfork_res_t* results, size_t count, size_t n);
static void sfork_run_all_limited(null_f_ptr_t*, sfork_res_t*, size_t, size_t);
static size_t count_done(sfork_res_t* results, size_t count);

// forks while maintaining a global static count of processes spawned so far
// handles a fork() failure internally (e.g. logs the error and calls exit(1))
static pid_t sfork(void)
{
    static u_int8_t forks = 1;
    const static u_int8_t FORK_LIMIT = 64;
    static pid_t SFORK_ROOT_PID = (pid_t)-1;
    if (forks == 1) {
        struct rlimit rlp;
        if (0 != getrlimit(RLIMIT_NPROC, &rlp)) {
            perror("getrlimit");
            exit(1);
        } else {
            printf("Current soft NPROC limit: %lu\n", (unsigned long)rlp.rlim_cur);

            printf("Hard NPROC limit: %lu\n", (unsigned long)rlp.rlim_max);
        }
        SFORK_ROOT_PID = getpid();
    }
    pid_t pid;
    if (forks*2 > FORK_LIMIT) { 
        if (getpid() == SFORK_ROOT_PID)
            fprintf(stderr, "Reached fork limit of %hhu, terminating processes.\n", FORK_LIMIT);
        exit(EXIT_FAILURE);
    }
    forks *= 2;

    // flush buffers in case a printf or w/e hasn't been flushed
    fflush(stderr);
    fflush(stdout);
    if (-1 == (pid = fork())) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    return pid;
}


// ensures that only the main root parent process will fork a new process
// doesn't completely prevent a fork bomb (in theory, if the function passed in
// does some evil longjmp shenanigans, or if passed-in-function itself calls the
// actual unlimited fork() function) but certainly makes it a lot harder to do
// so accidentally
static pid_t sfork_run(void f(void))
{
    const static u_int8_t SFORK_LIMIT = 64; // doesn't prevent fork bomb
    static size_t TOTAL_PROCESSES_FORKED = 0;
        // TODO: find out why this doesn't work
        // if (TOTAL_PROCESSES_FORKED == 0) { // first function call
        //     const struct rlimit rlp = { 32, 64 };
        //     if (0 != setrlimit(RLIMIT_NPROC, &rlp)) {
        //         perror("setrlimit");
        //         exit(1);
        //     } else {
        //         struct rlimit rlp_check;
        //         getrlimit(RLIMIT_NPROC, &rlp_check);
        //         printf("Current soft NPROC limit: %lu\n", (unsigned long)rlp_check.rlim_cur);

        //         printf("Hard NPROC limit: %lu\n", (unsigned long)rlp_check.rlim_max);
        //     }
        // }

    static pid_t SFORK_RUN_ROOT_PID = (pid_t)-1;
    if (SFORK_RUN_ROOT_PID <= 0) {
        SFORK_RUN_ROOT_PID = getpid();
    }

    if (TOTAL_PROCESSES_FORKED >= SFORK_LIMIT) {
        if (getpid() == SFORK_RUN_ROOT_PID)
            fprintf(stderr, "Reached sfork_run limit of %hhu total forked processes, terminating all processes.\n", SFORK_LIMIT);
        exit(EXIT_FAILURE);
    }

    // flush buffers in case a printf or w/e hasn't been flushed
    fflush(stderr);
    fflush(stdout);
    if (getpid() == SFORK_RUN_ROOT_PID) { // run this code only in root process
        pid_t pid = fork();
        if (pid == (pid_t)-1) {
            perror("fork");
            exit(EXIT_FAILURE);
        } else if (pid == (pid_t)0) { // root's child process
            f();
            exit(0);
        } else {
            TOTAL_PROCESSES_FORKED++;
            return pid; // return pid of child process to main parent only
        }
    } else {
        exit(0); // make sure all other child processes immediately exit
    }
}

// caller is responsible for freeing resulting test object
static sfork_res_t* sfork_init_results(size_t test_count)
{
    sfork_res_t* results = (sfork_res_t*)malloc(test_count * sizeof(sfork_res_t));
    for (size_t i = 0; i < test_count; i++) {
        
    }
    

 
    return results;
}

static void sfork_run_all(null_f_ptr_t* functions, sfork_res_t* results, size_t count)
{
    for (size_t i = 0; i < count; i++) {
        results[i].done = false;
        results[i].status = 0; 
        results[i].pid = sfork_run(functions[i]);
    }
}

static void sfork_run_all_limited(null_f_ptr_t* functions, sfork_res_t* results, size_t count, size_t limit)
{
    size_t running = 0;
    for (size_t i = 0; i < count; i++) {
        results[i].done = false;
        results[i].status = 0; 
        results[i].pid = sfork_run(functions[i]);
        running++;
        if (running >= limit) {
            wait_for_one_to_finish(results, count);
            running = count_done(results, i + 1);
        }
    }
}

static void sfork_reap_remaining(sfork_res_t* results, size_t count)
{
    size_t done = count_done(results, count);
    while (done++ < count) {
        wait_for_one_to_finish(results, count);
    }
}

static void wait_for_one_to_finish(sfork_res_t* results, size_t count) {

    int status_temp;
    pid_t finished = waitpid(-1, &status_temp, 0);
    if (finished == -1) {
        LOGERROR("waitpid error");
        exit(1);
    }
    // update results data for the finished process
    for (size_t i = 0; i < count; i++) {
        if (results[i].pid == finished) {
            results[i].status = status_temp; 
            results[i].done = true;
        }
    }
}

static size_t count_done(sfork_res_t* results, size_t count) {
    size_t done = 0; 
    for (size_t i = 0; i < count; i++) {
        if (results[i].done) { done++; }
    }
    return done;
}




#endif /* ifndef SFORK_H */




