#ifndef DMB_UTILS_H
#define DMB_UTILS_H

#include <errno.h>
#include <execinfo.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

#define UNUSED(x) (void)(x)
#define LOGERROR(message) do {\
    if (errno == 0) {\
        if (message[0] == '\0') {\
            fprintf(stderr, RED "Error" RESET " in " BLU "%s" RESET ":" YEL "%d" RESET "\n", __FILE__, __LINE__);\
        } else {\
            fprintf(stderr, RED "Error" RESET " in " BLU "%s" RESET ":" YEL "%d" RESET ": %s\n", __FILE__, __LINE__, message);\
        }\
    } else {\
        if (message[0] == '\0') {\
            fprintf(stderr, RED "Error" RESET " in " BLU "%s" RESET ":" YEL "%d" RESET ": " RED "errno " YEL "%d" RESET ": %s\n", __FILE__, __LINE__, errno, strerror(errno));\
        } else {\
            fprintf(stderr, RED "Error" RESET " in " BLU "%s" RESET ":" YEL "%d" RESET ": %s (" RED "errno " YEL "%d" RESET ": %s)\n", __FILE__, __LINE__, message, errno, strerror(errno));\
        }\
    }\
    void* callstack[128];\
    size_t frames = (size_t) backtrace(callstack, 128);\
    char** strs = backtrace_symbols(callstack, (int) frames);\
    for (size_t _log_error_i = 0; _log_error_i < frames; ++_log_error_i) { fprintf(stderr, "%s\n", strs[_log_error_i]); }\
    free(strs);\
    fflush(stderr);\
} while (0)
#define LOGEXIT(message) do {LOGERROR(message); exit(EXIT_FAILURE);} while (0)


static bool RAND_IS_SEEDED = false;

static void print_arr(char message[], int a[], size_t length);

static size_t rand_ul(size_t max);
static size_t* rand_arr(size_t len, size_t max_val);
static void randfill(int* arr, size_t len, size_t max_val);

static void millisleep(unsigned short);

static unsigned get_term_width( void );

/*****************************************************************************
 ***                           Implementations                             ***
 *****************************************************************************/
static void print_arr(char message[], int a[], size_t length)
{
    if (length == 1) {
        printf("%s[%d]", message, a[0]);
    } else {
        printf("%s%c", message, '[');
        for (size_t i = 0; i < length - 1; ++i) {
            printf("%d,", a[i]);
        }
        printf("%d]", a[length - 1]);
    }
}

static size_t rand_ul(size_t max)
{
    if (!RAND_IS_SEEDED) {
        struct timespec tp = { .tv_sec = 0, .tv_nsec = 0 };
        if (0 != clock_gettime(CLOCK_REALTIME, &tp)) {
            LOGERROR("failed to get clock time");
            exit(EXIT_FAILURE);
        }
        srand((unsigned) tp.tv_nsec); // this should only be called once EVER
        RAND_IS_SEEDED = true;
    }
    return (size_t) rand() % max;
}

static size_t* rand_arr(size_t len, size_t max_val)
{
    size_t *arr = (size_t*)malloc(len * sizeof(size_t));
    for (size_t i = 0; i < len; i++) {
        arr[i] = rand_ul(max_val);
    }

    return arr;
}

static void randfill(int* arr, size_t len, size_t max_val) {
    for (size_t i = 0; i < len; ++i) {
        arr[i] = (int) rand_ul(max_val);
    }
}

static void millisleep(unsigned short ms)
{
    unsigned short new_ms = ms % 1000;
    unsigned short s = ms / 1000;
    struct timespec sleeptime = {s, 1000000 * new_ms};
    struct timespec remain;
    int status;
    if (-1 == (status = nanosleep(&sleeptime, &remain)))
    {
        perror("Error calling nanosleep() in millisleep()");
        if (errno == EINTR)
        {
            nanosleep(&remain, NULL);
        }
    }
}

static unsigned get_term_width( void ) {
  struct winsize ws;
  if ( ioctl( STDIN_FILENO , TIOCGWINSZ, &ws ) != 0 &&
       ioctl( STDOUT_FILENO, TIOCGWINSZ, &ws ) != 0 &&
       ioctl( STDERR_FILENO, TIOCGWINSZ, &ws ) != 0 ) {
    fprintf( stderr, "ioctl() failed (%d): %s\n", errno, strerror( errno ) );
    return 0;
  }
  return ws.ws_col;
}

#endif /* end header guard DMB_UTILS_H */
