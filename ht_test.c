#include <stdio.h>
#include <assert.h>

#include "high_test.h"
#include "dmb_utils.h"

#define BLU   "\x1B[34m"
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define RESET "\x1B[0m"

typedef void(*void_f)(void);

void randsleep(void);
void exit_with_1(void) {
    randsleep();
    exit(1);
} 
void exit_with_3(void) {
    randsleep();
    exit(3);
} 
void segfault(void) {
    randsleep();
    int* i = NULL;
    i[2]++;
}
void throws(void) {
    randsleep();
    assert(1 == 2);
}
void fast(void) {
    return;
}
void aborts(void) {
    abort();
}
void stdout_spewer(void) {
    for (size_t i = 0; i < 5; i++) {
        printf("A bunch of stdout garbage\n");
    }
}

void stderr_spewer(void) {
    for (size_t i = 0; i < 5; i++) {
        fprintf(stderr, "A bunch of much longer and more annoying stderr garbage that takes up 80 chars %zu\n", i);
    }
}

void mega_spewer(void) {
    for (size_t i = 0; i < 1000; i++) {
        fprintf(stderr, "A bunch of much longer and more annoying stderr garbage that takes up 80 chars %zu\n", i);
    }
}


// void test_sfork_run_limited(void);

int main(void)
{
    should_abort("aborter", aborts);
    for (size_t i = 0; i < 8; i++) {
        should_just_pass("gotta go fast", fast);
    }
    for (size_t i = 0; i < 10; i++) {
        should_just_pass("mega_spewer", mega_spewer);
    }
    
    should_just_pass("mega_spewer", mega_spewer);
    for (size_t i = 0; i < 8; i++) {
        should_just_pass("stderr_spewer", stderr_spewer);
    }
    
    should_just_pass("stderr_spewer", stderr_spewer);
    should_just_pass("randsleep", randsleep);
    should_exit_with_code("exit(3)", exit_with_3, 3);
    should_exit_with_code("wrong_exit", exit_with_1, 3);
    should_abort("throws", throws);
    should_segfault("segfault", segfault);
    should_exit_with_signal("wrong_signal", throws, 12);
    run_tests();



    return 0;
}

void randsleep(void) {
    unsigned short duration = (unsigned short) rand_ul(2000);
    millisleep(duration);
    sleep(2);
}

// void slow_test(void) {
//     simple_test_and_print("slow_test", randsleep, 1);
// }

/* void test_sfork_run_limited(void) {
    printf("Testing" BLU "sfork_run_limited" RESET "...\n");
    size_t NUM_TESTS = 20;
    size_t MAX_ALLOWED_PROCESSES = 8;
    void_f* functions = (void_f*)malloc(NUM_TESTS * sizeof(void_f));
    for (size_t i = 0; i < NUM_TESTS; i++) {
        functions[i] = slow_test;
    }
    sfork_res_t* results = sfork_init_results(NUM_TESTS);
    sfork_run_all_limited(functions, results, NUM_TESTS, MAX_ALLOWED_PROCESSES);
    sfork_reap_remaining(results, NUM_TESTS);
    free(functions);
    free(results);
    t_result result = PASS;
    for (size_t i = 0; i < NUM_TESTS; i++) {
        if (!results[i].done) {
            result = FAIL;
        }
    }
    t_results final_results = { FAIL, 0, 0, 0 };
    final_results.pass = result;
    if (final_results.pass == FAIL) {
        printf(RED "Test failed." RESET "\n");
    } else {
        printf(GRN "Test passed!" RESET "\n");
    }
} */
