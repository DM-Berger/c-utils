CC=clang
CFLAGS=-Wall\
       -Wextra\
       -Wconversion\
       -Wfloat-equal\
       -Wformat=2\
       -Wimplicit-atomic-properties\
       -Wmissing-declarations\
       -Wno-missing-prototypes\
       -Woverlength-strings\
       -Wshadow\
       -Wstrict-selector-match\
       -Wundeclared-selector\
       -Wunreachable-code\
       -Wno-unused-parameter\
       -Wno-unused-function
CHECKFLAGS=-Wall\
	   -Werror\
	   -Wextra\
	   -Wconversion\
	   -Wfloat-equal\
	   -Wformat=2\
	   -Wimplicit-atomic-properties\
	   -Wmissing-declarations\
	   -Wno-missing-prototypes\
	   -Woverlength-strings\
	   -Wshadow\
	   -Wstrict-selector-match\
	   -Wundeclared-selector\
	   -Wunreachable-code\
	   -Wunused-parameter\
	   -Wunused-function

SRCS=ht_test.c utils_test.c
OBJS=${SRCS:.c=.o}
BINARIES=$(SRCS:.c=)

# Build everything in the directory
all: ${BINARIES}
	# @$(CC) -o utils_test utils_test.c $(CFLAGS)
	# @$(CC) -o high_test_test ht_test.c $(CFLAGS)

check: utils_test.c ht_test.c
	@$(CC) -g -o utils_test utils_test.c $(CHECKFLAGS)
	@$(CC) -g -o ht_test ht_test.c $(CHECKFLAGS)
# Build binaries
utils: utils_test.c
	@$(CC) -g -o utils_test utils_test.c $(CFLAGS)

high_test: high_test.c
	@$(CC) -c -g high_test.c $(CFLAGS)

ht_test_o: ht_test.c
	@$(CC) -c -g ht_test.c $(CFLAGS)

ht_test: high_test ht_test_o
	@$(CC) -g -o ht_test ht_test.o high_test.o $(CFLAGS)




test_utils: utils
	@./utils_test

test_ht: ht_test
	@./ht_test

test: test_utils test_ht
	
clean:
	rm -rf *.o

phony: all check utils high_test ht_test_o ht_test test_utils test_ht test clean
