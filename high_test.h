/*
******************************************************************************
* @file  
* @author   
* @version
* @date
* @brief
******************************************************************************

* The idea here will be to have a single process which watches a process pool of
* processes for segfaults and/or exits. Each test will get it's own child
* process to execute in, and that process will terminate upon successful (or
* unsuccesful) completion of the test. Premature termination (via either some
* non-zero and/or premature exit value, or by termination via signal (e.g.
* SIGSEGV)) can be caught via waitpid(). Proper termination will write the test
* results to a pipe, so the first parent process can use this info. 
*
* Additionally, it will be possible to set up:
*           should_fail(t_result(*f)(void)),
* and 
*           should_exit_with(t_result(*f)(void), int exit),
* and
*           should_segv(t_result(*f)(void))
*           tests. 
*
* For this to work will probably require all the "test_XXX" functions to
* actually just be "setup" functions that don't actually execute the tests they
* are passed, but rather push those functions onto some sort of event loop /
* list of functions to be executed.
*
* This will probably be implemented by having an array / list of all the
* necessary test info, and a queue / buffer of processes. After all the tests
* have been setup, the main process will iterate over the tests, starting each
* test in a new process. 
*
* Actually, we can do better, we don't need to implement a queue, we just need
* to make sure that the max number of processes running is limited by some
* global counter in main that tracks the number of running processes. Then as
* long as we decrement this whenever a process terminates, we will have a
* condition for creating new processes.
*
* However, this means we will need a sort of "process spawning" thread  in the
* main processes that just runs on a loop spawning processes whenever there are
* less than MAX_CHILD_PROCESSES running. Then the main process main thread can
* just basically waitpid() on *all* the processes, with something like 
        while (tests_not_done) {
            lock mutex on list of running processes;
            for (pid in running_processes) {
                waitpid()
            }
            unlock mutex;
        }
*
* Basically, the main process eneds a "test-runner" thread and "test-waiter"
* thread. The only info that has to be synchronized across these two threads is:
*           (1) the count of currently executing processes. 
* The test-runner thread needs to know *which* pids to currently wait on;
*
* Actually, the above won't work, because waitpid() blocks *the entire process*
* that calls it. So we actually need a test-runner *process* and and
* test-waiting *process*. In essence, if you want to limit the number of
* processes running, you are going to have to wait() once you have spawned that
* many processes (assuming process-spawning is faster than process executing).

Wait, actually NO!! waitpid(..., WNOHANG) only blocks rarely
*
* As a first pass, this will be implemented such that every test will have a new
* process that executes it, and which must be reaped. This is somewhat less than
* ideal, since creating processes is not at all cheap. Better would be to have a
* process pool of some size which only creates a new process when one of the
* processess is killed prematurely with either a signal or premature (nonzero)
* exit of some kind.





*/

#ifndef HIGH_TEST_H
#define HIGH_TEST_H

typedef enum _t_result { FAIL = 0, PASS = 1 } t_result ;

typedef struct _t_results {
    t_result pass;
    unsigned succ;
    unsigned fail;
    int status;
} t_results;

typedef void (*t_fn)(void);

void should_exit_with_code(char* label, t_fn f, int code);
void should_exit_with_signal(char* label, t_fn f, int signal);
void should_just_pass(char* label, t_fn f);
void should_segfault(char* label, t_fn f);
void should_abort(char* label, t_fn f);
void run_tests();
void set_test_repeats(unsigned count);
// void should_output(char* label, t_fn f, char* actual, char* exp);



#endif /* ifndef HIGH_TEST_H */
