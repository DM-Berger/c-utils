* leave `sfork.h` as a header file
* make `high_test.h` into a proper library
* pipe each test processes stdout and stderr to a buffer which is then
  printed only later when printing the summary for that test

* maybe look into using valgrind to check for memory leaks?
